#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

OPTIND=1

while getopts "d:u:p:s:h" opt; do
  case ${opt} in
    d)
      db_name="${OPTARG}"
      ;;
    u)
      db_user="${OPTARG}"
      ;;
    p)
      db_pass="${OPTARG}"
      ;;
    s)
      domain="${OPTARG}"
      ;;
    h|*)
      echo "-d [db_name] -u [db_user] -p [db_pass] -s [server / domain]"
      exit 2
      ;;
    \?)
      echo "Invalid option: -${OPTARG}."
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

shift $(( ${OPTIND} - 1 ))

[[ -z "${db_name}" ]] || [[ -z "${db_user}" ]] || [[ -z "${db_pass}" ]] || [[ -z "${domain}" ]] && exit 1

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

# Set timestamp (as count).
timestamp=$( date -u '+%Y-%m-%d.%H-%M-%S' )

# Set sleep time.
sleep="5"

# Get apps.
dump=$( which mysqldump )
php=$( which php )

# Set CMS.
cms_type="Drupal"
cms_user="root"
cms_pass=$( head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16 )
cms_mail="${cms_user}@localhost.local"
cms_name="${cms_type}: ${timestamp}"
cms_path="$( pwd )/public_html/web"

# -------------------------------------------------------------------------------------------------------------------- #
# Install.
# -------------------------------------------------------------------------------------------------------------------- #

if [[ ! -f "${cms_path}/index.php" ]]; then
  echo "File index.php DOES NOT exists!"
  exit 1
fi

# CMS: Core.
echo "--- Install ${cms_type}: Core | User: ${cms_user} | Password: ${cms_pass}"
SERVER_NAME=${domain}
export SERVER_NAME

${php} drupal                 \
--root="${cms_path}"          \
site:install standard         \
--langcode="en"               \
--db-type="mysql"             \
--db-host="127.0.0.1"         \
--db-name="${db_name}"        \
--db-user="${db_user}"        \
--db-pass="${db_pass}"        \
--db-port="3306"              \
--site-name="${cms_name}"     \
--site-mail="${cms_mail}"     \
--account-name="${cms_user}"  \
--account-mail="${cms_mail}"  \
--account-pass="${cms_pass}"

echo "" && sleep ${sleep}

# -------------------------------------------------------------------------------------------------------------------- #
# Exit.
# -------------------------------------------------------------------------------------------------------------------- #

exit 0
