%global app                     drupal
%global d_bin                   %{_bindir}

Name:                           meta-drupal
Version:                        1.0.0
Release:                        2%{?dist}
Summary:                        META-package for install and upgrade Drupal
License:                        GPLv3

Source10:                       %{app}
Source11:                       app.%{app}.install.sh

Requires:                       meta-system

%description
META-package for install and upgrade Drupal.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/%{app}
%{__install} -Dp -m 0755 %{SOURCE11} \
  %{buildroot}%{d_bin}/app.%{app}.install.sh


%files
%{d_bin}/%{app}
%{d_bin}/app.%{app}.install.sh


%changelog
* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-2
- UPD: Shell scripts.

* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
